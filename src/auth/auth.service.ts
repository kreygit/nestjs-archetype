import { Injectable, NotFoundException } from '@nestjs/common';
import { RegisterDto } from './dto/register.dto';
import { UsersService } from 'src/modules/users/users.service';
import { LoginCredentialsDto } from './dto/login-credentials.dto';
import * as bcrypt from 'bcrypt-nodejs';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  

  constructor(
    private userService: UsersService,
    private jwtService: JwtService
  ){}

  async register(registerData: RegisterDto) {
    const register = await this.userService.register(registerData);
    return register;
  }

  async login(credential: LoginCredentialsDto) {
    // Récupérer le login et le mot de passe
    const {login, password} = credential;
    // On peut se logger via le username ou l'email
    // Vérification si le login de l'utlisateur existe
    const user = await this.userService.findOneByUsernameOrEmailForLogin(login);
    // Vérication du mot de passe
    const hashedPassword = await bcrypt.hashSync(password, user.salt);
    if (hashedPassword === user.password) {
    const payload = {
      username: user.username,
      email: user.email,
      role: user.role
    };
    const jwt = await this.jwtService.sign(payload);
    return {
      'access_token': jwt
    };

    } else {
      // Si mot de passe incorrect je déclenche une erreur
      throw new NotFoundException(`Username ou password erronné`);
    }
  }
}

import { Body, Controller, Post } from "@nestjs/common";
import { ApiConflictResponse, ApiCreatedResponse, ApiNotFoundResponse, ApiTags } from "@nestjs/swagger";
import { AuthService } from "./auth.service";
import { LoginCredentialsDto } from "./dto/login-credentials.dto";
import { RegisterDto } from "./dto/register.dto";

@ApiTags('Auth')
@Controller('auth')
export class AuthController {

  constructor(
    private authService: AuthService
  ){}

  @Post('register')
  @ApiCreatedResponse({description: 'Retourne l\'utilisateur créé'})
  @ApiConflictResponse({description: 'Le username ou l\'email est déjà utilisé'})
  register(
    @Body() registerData: RegisterDto
  ) {
    return this.authService.register(registerData);
  }

  @Post('login')
  @ApiNotFoundResponse({description: 'Username ou password erronné'})
  login(
    @Body() credential: LoginCredentialsDto
  ) {
    return this.authService.login(credential);
  }
}
import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";

export class LoginCredentialsDto {

 @IsNotEmpty()
 @IsString()
 @ApiProperty({
  example: 'John Doe | john.doe@email.com',
  description: 'Le user de l\'utilisateur ou son email'
 })
 login: string;

 @IsNotEmpty()
 @IsString()
 @ApiProperty({
  example: 'monSup€rMot2P@sse',
  description: 'Le mot de passe d\'utilisateur'
})
 password: string;
}
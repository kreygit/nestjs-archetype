import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, IsString } from "class-validator";

export class RegisterDto {

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'John Doe',
    description: 'Le nom d\'utilisateur'
  })
  username: string;

  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({
    example: 'john.doe@email.com',
    description: 'L\'email de l\'utilisateur. Doit respecter le format e-mail'
})
  email: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'monSup€rMot2P@sse',
    description: 'Le mot de passe d\'utilisateur'
  })
  password: string;
}
import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PayloadInterface } from '../interfaces/payload.interface';
import { UsersService } from 'src/modules/users/users.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
   private configService: ConfigService,
   private userService: UsersService
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get('JWTKEY'),
    });
  }

  async validate(payload: PayloadInterface) {
   // Je récupère mon user
   const user = await this.userService.findOneByUsername(payload.username);
   // Si le user existe je le retourne et ce que je retourne dans validate
   // est mis dans le request
   if (user) {
    delete user.salt;
    delete user.password;
    delete user.createdAt;
    delete user.deletedAt;
    delete user.updatedAt;
    return user;
   } else {
    // Sinon je déclanche une erreur
    throw new UnauthorizedException();
   }
  }
}
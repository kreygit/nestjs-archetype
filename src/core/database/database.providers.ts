import { createConnection } from 'typeorm';
import { databaseConfig } from './database.config';
import { DEVELOPMENT, TEST, PRODUCTION } from '../constants';

let config;
switch (process.env.NODE_ENV) {
case DEVELOPMENT:
    config = databaseConfig.development;
    break;
case TEST:
    config = databaseConfig.test;
    break;
case PRODUCTION:
    config = databaseConfig.production;
    break;
default:
    config = databaseConfig.development;
}

export const databaseProviders = [
  {
    provide: 'DATABASE_CONNECTION',
    useFactory: async () => await createConnection({
      type: config.type,
      host: config.host,
      port: config.port,
      username: config.username,
      password: config.password,
      database: config.database,
      entities: [config.entities],
      synchronize : config.synchronize
    }),
  },
]
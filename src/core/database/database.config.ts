import * as dotenv from 'dotenv';
import { DbConfig } from './interfaces/db-config.interface';

dotenv.config();

export const databaseConfig: DbConfig = {
    development: {
        type: process.env.DB_TYPE,
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_DATABASE,
        entities: process.env.DB_ENTITIES,
        synchronize : true
    },
    test: {
        type: process.env.DB_TYPE,
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_DATABASE,
        entities: process.env.DB_ENTITIES,
        synchronize : false
    },
    production: {
        type: process.env.DB_TYPE,
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_DATABASE,
        entities: process.env.DB_ENTITIES,
        synchronize : false
    },
};
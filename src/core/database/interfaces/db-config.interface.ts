export interface DatabaseConfigAttributes {
  type?: string;
  host?: string;
  port?: number | string;
  username?: string;
  password?: string;
  database?: string;
  entities?: string;
  synchronize?: boolean;
}

export interface DbConfig {
  development: DatabaseConfigAttributes;
  test: DatabaseConfigAttributes;
  production: DatabaseConfigAttributes;
}

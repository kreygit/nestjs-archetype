import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { UserRoleEnum } from 'src/modules/enums/user-role.enum';

@Injectable()
export class IsAdminGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    if (request.user.role === UserRoleEnum.ADMIN) {
      return true;
    } else {
      throw new UnauthorizedException();
    }
    
  }

}

import { Body, Controller, Get, Param, Post, ParseIntPipe, Patch, Delete, UseGuards } from "@nestjs/common";
import { JwtAuthGuard } from "src/auth/guards/jwt-auth.guard";
import { IsAdminGuard } from "src/core/guards/is-admin.guard";
import { UserDto } from "./dto/user.dto";
import { UserEntity } from "./users.entity";
import { UsersService } from "./users.service";
import { ApiBearerAuth, ApiConsumes, ApiCreatedResponse, ApiExtraModels, ApiNotFoundResponse, ApiOkResponse, ApiOperation, ApiResponse, ApiTags, ApiUnauthorizedResponse} from '@nestjs/swagger';
import { User } from "src/core/decorators/user.decorator";

@ApiTags('User')
@Controller('user')
export class UserController {
    
  constructor(
    private usersService: UsersService,
  ) {}

  @Post()
  @ApiOperation({ description: 'Création d\'un utilisateur. UTILISER METHODE REGISTER DU MODULE AUTH', deprecated: true })
  @ApiCreatedResponse({description: 'Retourne l\'utilisateur créé'})
  @UseGuards(JwtAuthGuard)
  async create(
    @Body() addUserDto: UserDto
  ): Promise<UserEntity>{
    return this.usersService.create(addUserDto);
  }

  @Get()
  @ApiOkResponse({ description: "Renvoi la liste de tous les utilisateurs" })
  @UseGuards(JwtAuthGuard)
  async getAll(): Promise<UserEntity[]> {
    return this.usersService.findAll();
  }

  @Get(':id')
  @UseGuards(JwtAuthGuard)
  @ApiNotFoundResponse({description: `L\'utilisateur avec l'id ... n\'existe pas`})
  @ApiUnauthorizedResponse()
  async getOneById(
    @Param('id', ParseIntPipe) id: number
  ): Promise<UserEntity> {
    return await this.usersService.findOneById(id);
  }

  @Patch(':id')
  @UseGuards(JwtAuthGuard)
  @ApiNotFoundResponse({description: `L\'utilisateur avec l'id ... n\'existe pas`})
  @ApiUnauthorizedResponse()
  async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() userDto: UserDto,
    @User() user
    ): Promise<UserEntity> {
    return await this.usersService.update(id, userDto, user);
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  @ApiNotFoundResponse({description: `L\'utilisateur avec l'id ... n\'existe pas`})
  @ApiUnauthorizedResponse()
  async softRemove(
    @Param('id', ParseIntPipe) id: number
  ) {
    return await this.usersService.softRemove(id);
  }

  @Get('restore/:id')
  @UseGuards(JwtAuthGuard, IsAdminGuard) // L'ordre des guards est important, le JwtAuthGuard doit toujours être en premier
  @ApiNotFoundResponse({description: `L\'utilisateur avec l'id ... n\'existe pas`})
  @ApiUnauthorizedResponse()
  async restore(
    @Param('id', ParseIntPipe) id: number
  ) {
    return await this.usersService.restore(id);
  }
}
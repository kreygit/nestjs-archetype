import { ConflictException, Inject, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { RegisterDto } from 'src/auth/dto/register.dto';
import { getConnection, Repository } from 'typeorm';
import { UserDto } from './dto/user.dto';
import { UserEntity } from './users.entity'
import * as bcrypt from 'bcrypt-nodejs';
import { UserRoleEnum } from '../enums/user-role.enum';

@Injectable()
export class UsersService {

  constructor(
    @Inject('USER_REPOSITORY')
    private userRepository: Repository<UserEntity>,
  ) {}

  async create(user: UserDto): Promise<UserEntity> {
    return await this.userRepository.save(user);
  }

  async register(userData: RegisterDto): Promise<Partial<UserEntity>> {
    const user = this.userRepository.create({
      ...userData
    });
    user.salt = await bcrypt.genSaltSync();
    user.password = await bcrypt.hashSync(user.password, user.salt);
    try {
      await this.userRepository.save(user);
    } catch(e) {
      throw new ConflictException(`Le username ou l'email est déjà utilisé`);
    }
    return {
      id: user.id,
      username: user.username,
      email: user.email,
     };
  }

  async findAll(): Promise<UserEntity[]> {
    return await this.userRepository.find();
  }

  async findOneById(id: number): Promise<UserEntity> {
    const user = await this.userRepository.findOne(id);
    // Tester le cas où le user d'id id n'existe pas
    if (!user) {
      throw new NotFoundException(`Le user avec l'id ${id} n'éxiste pas`);
    } else {
      return user;
    }
  }

  async findOneByUsername(username: string): Promise<UserEntity> {
    const user = await this.userRepository.findOne({where: {username}});
    // Tester le cas où le username n'existe pas
    if (!user) {
      throw new NotFoundException(`Le user avec le username ${username} n'éxiste pas`);
    } else {
      return user;
    }
  }

  
  async findOneByUsernameOrEmail(login: string): Promise<UserEntity> {
    const user = await this.userRepository.findOne({
      where: [
        {username: login},
        {email: login }
      ]
    });
    if (!user) {
      throw new NotFoundException(`Le user avec le username ou email ${login} n'éxiste pas`)
    } else {
      return user;
    }
  }
  
  async findOneByUsernameOrEmailForLogin(login: string) {
    const user = await getConnection()
    .createQueryBuilder(UserEntity, 'user')
    .addSelect(['user.password', 'user.salt'])
    .where('user.username = :username', {username: login})
    .orWhere('user.email = :email', {email: login})
    .getOne();
    if (!user) {
      throw new NotFoundException(`Le user avec le username ou email ${login} n'éxiste pas`)
    } else {
      return user;
    }
  }

  async update(id: number, userDto: UserDto, user): Promise<UserEntity> {
    console.log('userDto: ', userDto);
    console.log('user: ', user);
    // On récupère le user d'id id et ensuite on remplace les anciennes valeurs de ce user
    // par ceux du user passé en paramètre
    const newUser = await this.userRepository.preload({
      id,
      ...userDto
    });
    console.log('newUser: ', newUser);
    // Tester le cas où le user d'id id n'existe pas
    if (!newUser) {
      throw new NotFoundException(`Le user avec l'id ${id} n'éxiste pas`);
    } else {
      if (user.role === UserRoleEnum.ADMIN || (user.id === newUser.id)) {
        return await this.userRepository.save(newUser);
      } else {
        throw new UnauthorizedException();
      }
    }
  }

  async softRemove(id: number) {
    const userToRemove = await this.userRepository.findOne({
      relations: ['blogs'],
      where: {id}
    });
    return await this.userRepository.softRemove(userToRemove);
  }

  async restore(id: number) {
    const userToRestore = await this.userRepository.find({ 
      relations: ['blogs'],
      where: {id},
      withDeleted: true
    });
    // Tester le cas où le user d'id id n'existe pas
    if (!userToRestore) {
      throw new NotFoundException(`Le user avec l'id ${id} n'éxiste pas`);
    } else {
      return await this.userRepository.restore(id);
    }
  }

  // Exemple d'une vraie suppression.
  async delete(id: number) {
    const userToDelete = await this.findOneById(id);
    return await this.userRepository.delete(id);
  }

  isOwnerOrAdmin(object, user) {
    return user.role === UserRoleEnum.ADMIN || (object.user && object.user.id === user.id);
   }
}
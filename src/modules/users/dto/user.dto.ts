import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, IsString } from "class-validator";
import { UserRoleEnum } from "src/modules/enums/user-role.enum";

export class UserDto {
    
    @IsNotEmpty()
    @IsString()
    @ApiProperty({
        example: 'John Doe',
        description: 'Le nom d\'utilisateur'
    })
    username: string;

    @IsNotEmpty()
    @IsEmail()
    @ApiProperty({
        example: 'john.doe@email.com',
        description: 'L\'email de l\'utilisateur. Doit respecter le format e-mail'
    })
    email: string;

    @IsString()
    @ApiProperty({
        example: 'user',
        description: 'Le role de l\'utilisateur',
        enum: UserRoleEnum,
        required: false,
        default: UserRoleEnum.USER
    })
    role: string;

}
import { Exclude } from 'class-transformer';
import { IsEmail } from 'class-validator';
import { TimestampEntities } from 'src/modules/generics/timestamp.entities';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { BlogEntity } from '../blogs/blogs.entity';
import { UserRoleEnum } from '../enums/user-role.enum';

@Entity('user')
export class UserEntity extends TimestampEntities {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'username',
    type: 'varchar',
    unique: true,
    comment: 'Nom de l\'utilisateur'
  })
  username: string;

  @Column({
    name: 'email',
    unique: true,
    comment: 'L\'email de l\'utilisateur'
  })
  @IsEmail()
  email: string;

  @Column({
    name: 'password',
    type: 'varchar',
    nullable: true,
    comment: 'Le mot de passe de l\'utilisateur',
    select: false
  })
  password: string;

  @Column({
    name: 'salt',
    type: 'varchar',
    nullable: true,
    comment: 'Salage pour renforcer la sécurité du mot de passe',
    select: false
  })
  salt: string;

  @Column({
    type: 'enum',
    enum: UserRoleEnum,
    default: UserRoleEnum.USER,
    comment: 'Le role de l\'utilisateur, par défaut: USER'
  })
  role: string;

  @OneToMany(
    () => BlogEntity,
    blogs => blogs.user,
    {
      nullable: true, 
      cascade: true
    }
  )
  blogs: BlogEntity[];
}
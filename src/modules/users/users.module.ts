import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { userProviders } from './users.providers';
import { DatabaseModule } from '../../core/database/database.module';
import { UserController } from './users.controller';

@Module({
  imports: [
    DatabaseModule,
  ],
  controllers: [UserController],
  providers: [
    ...userProviders,
    UsersService,
  ], exports: [UsersService]
})
export class UsersModule {}

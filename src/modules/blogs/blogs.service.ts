import { Inject, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { UserRoleEnum } from '../enums/user-role.enum';
import { UsersService } from '../users/users.service';
import { BlogEntity } from './blogs.entity';
import { AddBlogDto } from './dto/add-blog-dto';
import { UpdateBlogDto } from './dto/update-blog-dto';

@Injectable()
export class BlogsService {
 
  constructor(
    @Inject('BLOG_REPOSITORY')
    private blogRepository: Repository<BlogEntity>,
    private userService: UsersService
  ) {}

  async create(blog :AddBlogDto, user): Promise<BlogEntity> {
    const newBlog = this.blogRepository.create(blog);
    newBlog.user = user;
    return await this.blogRepository.save(newBlog);
  }

  async findAll(user): Promise<BlogEntity[]> {
    // L'utilisateur au profil admin récupère tous les blogs
    // L'utilisateur au profil user ne récupère que ses blogs
    if (user.role === UserRoleEnum.ADMIN) {
      return await this.blogRepository.find();
    } else {
      return await this.blogRepository.find({
        where: {user}
      });
    }
  }

  async findOneById(id: number, user): Promise<BlogEntity> {
    const blog = await this.blogRepository.findOne(id);
    if (!blog) {
      throw new NotFoundException(`Le blog avec l'id ${id} n\'existe pas`);
    }
    // Le blog est retourné si l'utilisateur est admin ou qu'il est l'auteur du blog
    if (this.userService.isOwnerOrAdmin(blog, user)) {
      return blog;
    } else {
      throw new UnauthorizedException();
    }
  }

  async updateCv(id: number, updateBlogDto: UpdateBlogDto, user): Promise<BlogEntity> {
    // On récupère le blog grace à l'id et on remplace les valeurs de ce blog
    // par ceux passé en paramètre
    const updatedBlog = await this.blogRepository.preload({
      id,
      ...updateBlogDto
    });
    // Test si le blog existe
    if (!updatedBlog) {
      throw new NotFoundException(`Le blog avec l'id ${id} n\'existe pas`);
    }
    if (this.userService.isOwnerOrAdmin(updatedBlog, user)) {
      return await this.blogRepository.save(updatedBlog);
    } else {
      throw new UnauthorizedException();
    }
  }

  async delete(id: number, user) {
    const blogToRemove = await this.findOneById(id, user);
    if (this.userService.isOwnerOrAdmin(blogToRemove, user)){
      return await this.blogRepository.softRemove(blogToRemove);
    } else {
      throw new UnauthorizedException()
    }
  }

  async restore(id: number) {
    const blogToRestore = await this.blogRepository.find({ where: {id}, withDeleted: true});
    // Tester le cas où le user d'id id n'existe pas
    if (!blogToRestore) {
      throw new NotFoundException(`Le user avec l'id ${id} n'éxiste pas`);
    } else {
      return await this.blogRepository.restore(id);
    }
  }
}
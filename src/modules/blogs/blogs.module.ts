import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/core/database/database.module';
import { BlogsController } from './blogs.controller';
import { BlogsService } from './blogs.service';
import { blogProviders } from './blogs.providers';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    DatabaseModule,
    UsersModule,
  ],
  controllers: [BlogsController],
  providers: [
    ...blogProviders,
    BlogsService
  ]
})
export class BlogsModule {}

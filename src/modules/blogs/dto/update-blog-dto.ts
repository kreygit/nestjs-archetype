import { IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";
import { Type } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";

export class UpdateBlogDto {

    @IsOptional()
    @IsString()
    @ApiProperty({
        example: 'Mon blog',
        description: 'Le nom du blog.',
        required: false
    })
    name: string;

    @IsOptional()
    @IsString()
    @ApiProperty({
        example: 'Mon contenu du blog',
        description: 'Le contenu du blog.',
        required: false
    })
    content: string;

}
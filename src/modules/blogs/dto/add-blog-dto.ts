import { IsNotEmpty, IsNumber, IsString } from "class-validator";
import { Type } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";

export class AddBlogDto {

    @IsNotEmpty()
    @IsString()
    @ApiProperty({
        example: 'Mon blog',
        description: 'Le nom du blog'
    })
    name: string;

    @IsNotEmpty()
    @IsString()
    @ApiProperty({
        example: 'Mon contenu du blog',
        description: 'Le contenu du blog'
    })
    content: string;

}
import { Connection } from 'typeorm';
import { BlogEntity } from './blogs.entity';

export const blogProviders = [
  {
    provide: 'BLOG_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(BlogEntity),
    inject: ['DATABASE_CONNECTION'],
  },
];
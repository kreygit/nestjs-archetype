import { Column, Entity, JoinTable, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { TimestampEntities } from "../generics/timestamp.entities";
import { UserEntity } from "../users/users.entity";

@Entity('blog')
export class BlogEntity extends TimestampEntities {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'name',
    type: 'varchar',
    unique: true,
    comment: 'Nom du blog'
  })
  name: string;

  @Column({
    name: 'content',
    type: 'varchar',
    nullable: false,
    comment: 'Le contenu du blog'
  })
  content: string;

  @ManyToOne(
    () => UserEntity,
    user => user.blogs,
    {
      nullable: true,
      eager: true
    }
  )
  user: UserEntity;

}
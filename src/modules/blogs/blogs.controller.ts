import { Body, Controller, Get, Patch, Param, Post, UseGuards, Delete, ParseIntPipe } from '@nestjs/common';
import { ApiCreatedResponse, ApiNotFoundResponse, ApiOkResponse, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { User } from 'src/core/decorators/user.decorator';
import { IsAdminGuard } from 'src/core/guards/is-admin.guard';
import { BlogEntity } from './blogs.entity';
import { BlogsService } from './blogs.service';
import { AddBlogDto } from './dto/add-blog-dto';
import { UpdateBlogDto } from './dto/update-blog-dto';

@ApiTags('Blog')
@Controller('blog')
export class BlogsController {

  constructor(
    private blogsService: BlogsService
  ) {}

  @Post()
  @UseGuards(JwtAuthGuard)
  @ApiCreatedResponse({description: 'Retourne le blog créé'})
  async create(
    @Body() blogDto: AddBlogDto,
    @User() user
  ): Promise<BlogEntity> {
    return await this.blogsService.create(blogDto, user);
  }

  @Get()
  @UseGuards(JwtAuthGuard)
  @ApiOkResponse({description: 'Si profil admin -> renvoi tous les blogs, si profil user -> renvoi tous les blogs de l\'utilisateur'})
  async findAll(
    @User() user
  ): Promise<BlogEntity[]> {
    return this.blogsService.findAll(user);
  }

  @Get(':id')
  @UseGuards(JwtAuthGuard)
  @ApiNotFoundResponse({description: `Le blog avec l'id ... n\'existe pas`})
  @ApiUnauthorizedResponse()
  async findOneById(
    @Param('id', ParseIntPipe) id: number,
    @User() user
  ): Promise<BlogEntity> {
    return this.blogsService.findOneById(id, user);
  }

  @Patch(':id')
  @UseGuards(JwtAuthGuard)
  @ApiNotFoundResponse({description: `Le blog avec l'id ... n\'existe pas`})
  @ApiUnauthorizedResponse()
  async updateBlog(
    @Body() updateBlogDto: UpdateBlogDto,
    @Param('id', ParseIntPipe) id: number,
    @User() user
  ): Promise<BlogEntity> {
    return await this.blogsService.updateCv(id, updateBlogDto, user);
 }

  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  @ApiNotFoundResponse({description: `Le blog avec l'id ... n\'existe pas`})
  @ApiUnauthorizedResponse()
  async delete(
    @Param('id', ParseIntPipe) id: number,
    @User() user
  ) {
    return await this.blogsService.delete(id, user);
  }

  @Get('restore/:id')
  @UseGuards(JwtAuthGuard, IsAdminGuard) // L'ordre des guards est important, le JwtAuthGuard doit toujours être en premier
  @ApiNotFoundResponse({description: `Le blog avec l'id ... n\'existe pas`})
  @ApiUnauthorizedResponse()
  async restore(
    @Param('id', ParseIntPipe) id: number,
    @User() user
  ) {
    return await this.blogsService.restore(id);
  }
}
import { CreateDateColumn, UpdateDateColumn, DeleteDateColumn } from "typeorm";

export class TimestampEntities {
 
 @CreateDateColumn({
  update: false,
  comment: 'Date de création de l\'entité'
 })
 createdAt: Date;

 @UpdateDateColumn({
    comment: 'Date de modification de l\'entité'
 })
 updatedAt: Date;

 @DeleteDateColumn({
     comment: 'Date de suppression de l\'entité'
 }
 )
 deletedAt: Date;
 
}